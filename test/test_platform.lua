local function get_os_name()
    local handle = io.popen("uname -s")
    local result = handle:read("*a")
    handle:close()
    return result:lower()
end

local os_name = get_os_name()
if os_name:match("darwin") then
    print("This is macOS.")
elseif os_name:match("linux") then
    print("This is Linux.")
else
    print("Unknown OS: " .. os_name)
end

