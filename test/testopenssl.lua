local skynet = require "skynet"
local openssl = require "openssl"


--short encrypt/decrypt
local evp_cipher = openssl.cipher.get('des')
m = 'abcdefghick'
key = m
cdata = evp_cipher:encrypt(m,key)
m1  = evp_cipher:decrypt(cdata,key)
print("m1:"..m1)
assert(m==m1)

--quick evp_digest
md = openssl.digest.get('md5')
m = 'abcd'
aa = md:digest(m)

mdc=md:new()
mdc:update(m)
bb = mdc:final()
assert(openssl.hex(aa,true)==bb)

-- Quick HMAC hash
local hmac = require "openssl".hmac

alg = 'sha256'
key = '0123456789'
msg = 'example message'

hmac.hmac(alg, msg, key, true) -- binary/"raw" output
hmac.hmac(alg, msg, key, false) -- hex output


-- Example 7: aes-128-gcm

local openssl = require('openssl')

local evp = openssl.cipher.get('aes-128-gcm')
local info = evp:info()

-- get cipher info
local key = openssl.random(info.key_length)
local msg = openssl.random(info.key_length)
local iv = openssl.random(info.iv_length)
local tn = 16 -- tag length

--do encrypt
local e = evp:encrypt_new()
assert(e:ctrl(openssl.cipher.EVP_CTRL_GCM_SET_IVLEN, #iv))
assert(e:init(key, iv))
e:padding(false)

--- get cipher
local c = assert(e:update(msg))
c = c .. e:final()
assert(#c==#msg)

--- Get the tag
local tag = assert(e:ctrl(openssl.cipher.EVP_CTRL_GCM_GET_TAG, tn))
assert(#tag==tn)

--do decrypt
e = evp:decrypt_new()
assert(e:ctrl(openssl.cipher.EVP_CTRL_GCM_SET_IVLEN, #iv))
assert(e:init(key, iv))
e:padding(false)

local r = assert(e:update(c))
assert(e:ctrl(openssl.cipher.EVP_CTRL_GCM_SET_TAG, tag))
r = r .. assert(e:final())
assert(#r==#c)

assert(r==msg)
print('Done')



skynet.start(skynet.exit)