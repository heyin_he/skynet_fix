local skynet = require "skynet"
local sprotoloader = require "sprotoloader"

local max_client = 64

skynet.start(function()
	skynet.error("Server start")
	skynet.uniqueservice("protoloader")
	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	skynet.newservice("debug_console",8000)
	skynet.newservice("simpledb")
	-- skynet.newservice("testopenssl") -- 安装openssl 可以测试
	local watchdog = skynet.newservice("watchdog") -- 会启动skynet.start( 服务
	skynet.error(watchdog)
	local addr,port = skynet.call(watchdog, "lua", "start", { -- 阻塞api
		port = 8888,
		maxclient = max_client,
		nodelay = true,
	})
	skynet.error("Watchdog listen on " .. addr .. ":" .. port)
	skynet.exit()
end)
