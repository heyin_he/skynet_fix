local skynet = require "skynet"

local CMD = {}
local SOCKET = {}
local gate
local agent = {}


local function printTable(o)
	if not o then
		return ""
	end
	print("type:"..type(o))
	if type(o) == 'table' then
	   local s = '{ '
	   for k,v in pairs(o) do
		  if type(k) ~= 'number' then k = '"'..k..'"' end
		  s = s .. '['..k..'] = ' .. printTable(v) .. ','
	   end
	   return s .. '} '
	else
	   return tostring(o)
	end
 end


function SOCKET.open(fd, addr)
	skynet.error("新的socket链接 client from : " .. addr.." fd:"..fd)
	agent[fd] = skynet.newservice("agent")
	skynet.error("打开agent。。。"..addr)
	skynet.call(agent[fd], "lua", "start", { gate = gate, client = fd, watchdog = skynet.self() })
end

local function close_agent(fd)
	local a = agent[fd]
	agent[fd] = nil
	if a then
		skynet.call(gate, "lua", "kick", fd)
		-- disconnect never return
		skynet.send(a, "lua", "disconnect")
	end
end

function SOCKET.close(fd)
	print("socket close",fd)
	close_agent(fd)
end

function SOCKET.error(fd, msg)
	print("socket error",fd, msg)
	close_agent(fd)
end

function SOCKET.warning(fd, size)
	-- size K bytes havn't send out in fd
	print("socket warning", fd, size)
end

function SOCKET.data(fd, msg)
end

function CMD.start(conf)
	skynet.error(conf)
	skynet.error("启动gate。。。")
	return skynet.call(gate, "lua", "open" , conf)
end

function CMD.close(fd)
	close_agent(fd)
end

skynet.start(function()
	skynet.error("开始。。。")
	skynet.dispatch("lua", function(session, source, cmd, subcmd, ...)
		skynet.error("watch dog 接收到消息 session:"..session.." source:"..string.format("%x",source).." cmd:"..cmd.."")
		
		if subcmd then
			skynet.error("subcmd:"..printTable(subcmd))
		end
		
		if cmd == "socket" then
			local f = SOCKET[subcmd]
			f(...)
			-- socket api don't need return
		else
			local f = assert(CMD[cmd])
			
			skynet.ret(skynet.pack(f(subcmd, ...)))
		end
	end)

	gate = skynet.newservice("gate")
end)
