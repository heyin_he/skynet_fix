Fork 的项目目录：

git clone https://github.com/cloudwu/skynet.git
cd skynet
make 'PLATFORM'  # PLATFORM can be linux, macosx, freebsd now

ubuntu22:
    #先安装
    sudo apt install build-essential
    sudo apt-get install git autoconf

当前fork版本:
    1、cjson 库添加到 3rd 目录下 ， git clone https://github.com/cloudwu/lua-cjson.git,并且修改支持cjson.so 在 Makefile文件里, 最后用了这个 https://github.com/openresty/lua-cjson.git 因为里面有 cjson.empty_array_mt
    
    2、openssl 库添加到 3rd 目录下 ，git clone --recurse https://github.com/zhaozg/lua-openssl.git lua-openssl 但是没有编译成功，所以，是直接拿     luarocks install openssl 后 拿 openssl.so 放到 luaclib 目录下的,可能 mac 和linux 不一样，需要重新 生成个 openssl.so 文件吧
